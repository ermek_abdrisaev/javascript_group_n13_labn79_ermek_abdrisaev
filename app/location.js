const express = require('express');
const db = require("../fileDb");
const router = express.Router();
const pathURL = './dbCat.json';

router.get('/', async (req, res) =>{
  await db.init(pathURL);
  const location = db.getItems();
  return res.send(location);
});

router.get('/:id', async (req, res) =>{
  await db.init(pathURL);
  const location = db.getItem(req.params.id);

  if(!location){
    return res.status(404).send({message: 'Not found'});
  }
  return res.send(location);
});

router.post('/', async (req, res, next) =>{
  try {
    if (!req.body.name || !req.body.description) {
      return res.status(400).send({message: 'Location name and description are required'});
    }
    await db.init(pathURL);
    const location = {
      name: req.body.name,
      description: req.body.description,
    }
    await db.addItem(location);

    return res.send({message: 'Created new location', id: location.id});
  } catch (e) {
    next(e);
  }
});

router.put('/:id', async (req, res) =>{
  await db.init(pathURL);
  res.send('Editing of new location');
});

router.delete('/:id', async (req, res) =>{
  await db.init(pathURL);
  res.send('Deleting of location');
});

module.exports = router;