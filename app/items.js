const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require("nanoid");
const config = require('../config');
const db = require('../fileDb');
const pathURL = './db.json';

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get('/', async (req, res) => {
  await db.init(pathURL);
  const items = db.getItems();
  let separatedItem = items.map(item => {
    return {
      id: item.Id,
      name: item.name
    }
  })
  return res.send(separatedItem);
});

router.get('/:id', async (req, res) => {
  await db.init(pathURL);
  const item = db.getItems(req.params.id);
  if (!item) {
    return res.status(404).send({message: 'Not found'});
  }

  return res.send(item);
});

router.post('/', upload.single('image'), async (req, res, next) => {
  try {
    if (!req.body.name || !req.body.description) {
      return res.status(400).send({message: 'Category name and description are required'});
    }
    await db.init(pathURL);
    const item = {
      name: req.body.name,
      description: req.body.description,
      image: null,
      categoryId: req.body.categoryId,
      locationId: req.body.locationId,
    }

    if (req.file) {
      item.image = req.file.filename;
    }

    await db.addItem(item);

    return res.send({message: 'Created new item', id: item.id});
  } catch (e) {
    next(e);
  }
  res.send('Creation of new item');
});

router.put('/', async (req, res) => {
  await db.init(pathURL);
  const item = db.getItem(req.params.id);
  console.log(item)
  if (!item) {
    return res.status(404).send('No such category!');
  }else{
    const newItem ={
      name: item.name,
      description: item.description,
      image: item.image,
      categoryId: item.categoryId,
      locationId: item.locationId,
    }

    // if(req.file){
    //   newItem.image = req.body.image;
    // }

    res.send(newItem);
  }

});

router.delete('/:id', async (req, res) => {
  await db.init(pathURL);
  res.send('Deleting of item');
});

module.exports = router;