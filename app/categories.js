const express = require('express');
const db = require('../fileDb');
const pathURL = './db.json';

const router = express.Router();

router.get('/', async (req, res) =>{
  await db.init(pathURL);
  const categories = db.getItems();
  let separatedCategory = categories.map( item => {
    return {
      id: item.Id,
      name: item.name
    }
  });
  return res.send(separatedCategory);
});

router.get('/:id', async (req, res) =>{
  await db.init(pathURL);
  const category = db.getItem(req.params.id);
  console.log(category);
  if(!category){
    return res.status(404).send({message: 'Not found'});
  }
  return res.send(category);
});

router.post('/',  async (req, res, next) => {
  try {
    if (!req.body.name || !req.body.description) {
      return res.status(400).send({message: 'Category name and description are required'});
    }
    await db.init(pathURL);
    const category = {
      name: req.body.name,
      description: req.body.description,
    }
    await db.addItem(category);

    return res.send({message: 'Created new category', id: category.id});
  } catch (e) {
    next(e);
  }
});

router.put('/:id', async (req, res) =>{
  await db.init(pathURL);
  const category = db.getItem(req.params.id);
  if(!category){
    return res.status(404).send('No such category!');
  }
  category.name = req.body.name;
  category.description = req.body.description;
  res.send({message: 'Category edited', id: category.id});
});

router.delete('/category/:id', async (req, res) =>{
  await db.init(pathURL);
  const id = db.getItem(req.params.id);

  categories = categories.filter(i =>{
    if(i.id !== id){
      return db.deleteItem(id);
    }
    return false;
  });

  return res.delete('Category deleted');
});

module.exports = router;