const express = require('express');
const db = require('./fileDb');
const categories = require('./app/categories');
const location = require('./app/location');
const items = require('./app/items');
const app = express();

const port = 8080;

app.use(express.json());

app.use('/categories', categories);
app.use('/locations', location);
app.use('/items', items)

const run = async () =>{
  await db.init();

  app.listen(port, () =>{
    console.log(`Server started on ${port} port!`);
  });
}

run().catch(e => console.error(e));


