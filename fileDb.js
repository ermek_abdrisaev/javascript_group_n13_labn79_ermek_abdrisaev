const fs = require('fs').promises;
const {nanoid} = require("nanoid");

let filename = './dbCat.json';
let data = [];

module.exports = {
  async init(path){
    try{
      filename = path;
      const fileContents = await fs.readFile(filename);
      data = JSON.parse(fileContents.toString());
    } catch(e){
      data = [];
    }
  },
  getItems(){
    return data;
  },
  getItem(id){
    return data.find(p => p.id === id);

  },
  addItem(item){
    item.Id = nanoid();
    data.push(item);
    return this.save();
  },
  save(){
    return fs.writeFile(filename, JSON.stringify(data, null, 2));
  },
  deleteItem(id){
    return data.find(p => p.id === id);
  }
}